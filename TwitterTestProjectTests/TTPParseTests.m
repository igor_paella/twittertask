//
//  TTPParseTests.m
//  TwitterTestProject
//
//  Created by Игорь on 28/04/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "TTPParser.h"

@interface TTPParseTests : XCTestCase

@end

@implementation TTPParseTests

- (void)testParseSuccess {
    // Arrange
    TTPParser *parser = [TTPParser new];
    NSDictionary *dict = @{@"id_str":@"123456", @"text":@"test text", @"created_at":@"Tue Apr 28 13:29:05 +0000 2015", @"user":@{@"name":@"igor", @"profile_image_url_https":@""}};
    
    // Act
    TTPTweet *tweet = [parser parseTweetWithDictionary:dict];
    
    // Assert
    XCTAssertEqualObjects(tweet.id, @"123456");
    XCTAssertEqualObjects(tweet.text, @"test text");
    XCTAssertEqualObjects(tweet.userName, @"igor");
    XCTAssertEqualObjects(tweet.date.description, @"2015-04-28 13:29:05 +0000");
}

- (void)testParseEmptyDictionary {
    // Arrange
    TTPParser *parser = [TTPParser new];
    NSDictionary *dict = [NSDictionary new];
    
    // Act
    TTPTweet *tweet = [parser parseTweetWithDictionary:dict];
    
    // Assert
    XCTAssertNil(tweet.id);
    XCTAssertNil(tweet.text);
    XCTAssertNil(tweet.userName);
    XCTAssertNil(tweet.date.description);
}


- (void)testParsePerformance {
    TTPParser *parser = [TTPParser new];
    NSDictionary *dict = @{@"id_str":@"123456", @"text":@"test text", @"created_at":@"Tue Apr 28 13:29:05 +0000 2015", @"user":@{@"name":@"igor", @"profile_image_url_https":@""}};

    [self measureBlock:^{
        TTPTweet *tweet = [parser parseTweetWithDictionary:dict];
        XCTAssertEqualObjects(tweet.id, @"123456");
    }];
}

@end
