//
//  TTPNetworkClient.m
//  TwitterTestProject
//
//  Created by Игорь on 27/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import "TTPNetworkClient.h"
#import <UIKit/UIKit.h>

@implementation TTPNetworkClient

static NSString * const kBaseUrl = @"https://api.twitter.com/1.1";
static NSString * const kCustomerKey = @"lp5xAn4nzs2f9xmX0AL3pWGQY";
static NSString * const kCustomerSecret = @"ex5f8V0mRdqS32EBuRKl9jp9IhazbRAl6pPDGJkNm4pkB3bOBS";

- (void)updateBearerToken:(void(^)(NSString*))callback {
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/oauth2/token"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:0.0];
    [request addValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    NSData *postData = [@"grant_type=client_credentials" dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    
    NSString *authStr = [self _encodeBase64WithString:[NSString stringWithFormat:@"%@:%@", kCustomerKey, kCustomerSecret]];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", authStr];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        if (json) {
            NSString *accessToken = json[@"access_token"];
            if (accessToken) {
                callback(accessToken);
            }
        }
    }];
    [task resume];
}

- (void)getTweetsForQuery:(NSString*)query withBearer:(NSString*)bearer withCallback:(void(^)(NSDictionary*))callback {
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", kBaseUrl, @"/search/tweets.json?q=", query]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:0.0];
    [request setHTTPMethod:@"GET"];
    NSString *authValue = [NSString stringWithFormat:@"Bearer %@", bearer];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
        if (httpResp.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            callback(json);
        }
    }];
    [task resume];
}

#pragma mark -
#pragma mark Private

- (NSString*)_encodeBase64WithString:(NSString*)string {
    NSData *stringData = [string dataUsingEncoding:NSUTF8StringEncoding];
    return [stringData base64EncodedStringWithOptions:0];
}

@end