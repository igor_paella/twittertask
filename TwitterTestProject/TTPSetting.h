//
//  TTPSetting.h
//  TwitterTestProject
//
//  Created by Игорь on 09/04/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTPSetting : NSObject

@property (nonatomic, assign) NSInteger key;
@property (nonatomic, assign) NSInteger value;

- (BOOL)boolValue;

@end
