//
//  TTPNetworkClient.h
//  TwitterTestProject
//
//  Created by Игорь on 27/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTPNetworkClient : NSObject

/**
 Get bearer token for Application-only authentication. For details - https://dev.twitter.com/oauth/application-only
 @param callback A block object to be executed when request finishes successfully. Argument: auth token.
 */
- (void)updateBearerToken:(void(^)(NSString*))callback;

/**
 Get tweets for query string from network server.
 @param query Query string.
 @param bearer Auth token.
 @param withCallback A block object to be executed when request finishes successfully. Argument: response dictionary.
 */
- (void)getTweetsForQuery:(NSString*)query withBearer:(NSString*)bearer withCallback:(void(^)(NSDictionary*))callback;

@end
