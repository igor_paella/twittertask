//
//  TTPParser.m
//  TwitterTestProject
//
//  Created by Игорь on 28/04/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import "TTPParser.h"

@implementation TTPParser {
    NSDateFormatter *_formatter;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    _formatter = [NSDateFormatter new];
    [_formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [_formatter setDateFormat:@"eee MMM dd HH:mm:ss ZZZZ yyyy"]; // Sat Mar 28 14:04:21 +0000 2015
}

- (TTPTweet*)parseTweetWithDictionary:(NSDictionary*)dict {
    TTPTweet *tweet = [TTPTweet new];
    tweet.id = dict[@"id_str"];
    tweet.text = dict[@"text"];
    tweet.date = [_formatter dateFromString:dict[@"created_at"]];
    NSDictionary *user = (NSDictionary*)dict[@"user"];
    tweet.userName = user[@"name"];
    tweet.userAvatarUrl = user[@"profile_image_url_https"];
    return tweet;
}

@end
