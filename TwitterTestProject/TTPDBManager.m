//
//  TTPDBManager.m
//  TwitterTestProject
//
//  Created by Игорь on 28/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import "TTPDBManager.h"
#import "FMDatabase.h"

@implementation TTPDBManager

static NSString * const kDbFileName = @"TwitterTest.sqlite";

+ (void)checkCopyDatabase {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *databasePath = [TTPDBManager pathToDB];
    BOOL success = [fileManager fileExistsAtPath:databasePath];
    if (!success) {
        NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:kDbFileName];
        [fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
        [self _createDatabaseSchema];
    }
}

+ (NSString*)pathToDB {
    static dispatch_once_t dispatchToken;
    static NSString *path;
    dispatch_once(&dispatchToken, ^{
        NSArray *supportPaths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
        NSString *supportDir = [supportPaths objectAtIndex:0];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if (![fileManager fileExistsAtPath:supportDir]) {
            [fileManager createDirectoryAtPath:supportDir withIntermediateDirectories:NO attributes:nil error:nil];
        }
        path = [supportDir stringByAppendingPathComponent:kDbFileName];
    });
    return path;
}

+ (void)_createDatabaseSchema {
    FMDatabase *db = [FMDatabase databaseWithPath:[TTPDBManager pathToDB]];
    NSString *sql = @"create table Tweets (id text primary key, userName text, userAvatarUrl text, text text, date date);"
    "create table Settings (key integer primary key, value integer);"
    "create table ImageCache (url text primary key, data data)";
    [db open];
    [db executeStatements:sql];
    [db close];
}

@end
