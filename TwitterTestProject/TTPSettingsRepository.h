//
//  TTPSettingsRepository.h
//  TwitterTestProject
//
//  Created by Игорь on 28/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTPBaseRepository.h"
#import "TTPSetting.h"

typedef NS_ENUM(NSInteger, TTPSettingsKey) {
    TTPSettingsKeyShowAvatars
};


@interface TTPSettingsRepository : TTPBaseRepository

/**
 Get setting from storage.
 @param key Value of TTPSettingsKey enum.
 @param withDefault Default value if key not exist in storage.
 @return TTPSetting.
 */
- (TTPSetting*)getSettingForKey:(TTPSettingsKey)key withDefault:(NSNumber*)defaultValue;

/**
 Save setting to storage.
 @param value Value for setting.
 @param forKey Value of TTPSettingsKey enum.
 */
- (void)setSettingValue:(NSNumber*)value forKey:(TTPSettingsKey)key;

@end
