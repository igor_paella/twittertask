//
//  TTPTweetsViewController.m
//  TwitterTestProject
//
//  Created by Игорь on 27/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import "TTPTweetsViewController.h"
#import "TTPSettingsRepository.h"
#import "TTPModelFacade.h"
#import "TTPTweet.h"
#import "TTPTweetCell.h"

@implementation TTPTweetsViewController {
    TTPSettingsRepository *_settings;
    NSArray *_tweets;
    UILabel *_leftItemView;
    NSTimer *_timer;
    BOOL _showAvatars;
    int _valueToUpdate;
}

static NSString * const kRequestString = @"@twitterapi";
static NSString * const kTweetCellIdentifier = @"TweetCellIdentifier";
static const int kUpdateTimeout = 60;
static const CGFloat kDefaultCellHeight = 76.0f;

- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    _valueToUpdate = kUpdateTimeout;
    _settings = [TTPSettingsRepository new];
    _tweets = [NSArray new];
}

#pragma mark -
#pragma mark Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = kRequestString;
    _leftItemView = [[UILabel alloc] initWithFrame:CGRectMake(0,0,30,30)];
    _leftItemView.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_leftItemView];
    // remove empty cells separators
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _showAvatars = [_settings getSettingForKey:TTPSettingsKeyShowAvatars withDefault:[NSNumber numberWithBool: YES]].boolValue;
    [self _loadTweets];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    BOOL showAvatarNew = [_settings getSettingForKey:TTPSettingsKeyShowAvatars withDefault:[NSNumber numberWithBool: YES]].boolValue;
    BOOL needUpdate = _showAvatars != showAvatarNew;
    _showAvatars = showAvatarNew;
    if (needUpdate) {
        [self _updateVisibleCells];
    }
    
    _timer = [NSTimer timerWithTimeInterval:1.0
                                     target:self
                                   selector:@selector(onTick:)
                                   userInfo:nil
                                    repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [_timer invalidate];
    _timer = nil;
}

- (void)onTick:(NSTimer*)timer {
    _valueToUpdate--;
    if (_valueToUpdate <= 0) {
        _valueToUpdate = kUpdateTimeout;
        [self _loadTweets];
    }
    _leftItemView.text = [NSString stringWithFormat:@"%d",_valueToUpdate];
}

#pragma mark -
#pragma mark TableViewSource

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    return _tweets.count;
}

- (UITableViewCell *)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {

    TTPTweetCell *cell = (TTPTweetCell*)[tableView dequeueReusableCellWithIdentifier:kTweetCellIdentifier];
    
    TTPTweet *tweet = [_tweets objectAtIndex:indexPath.row];
    [cell configure:tweet showAvatar:_showAvatars];
    return cell;
}

#pragma mark -
#pragma mark TableViewDelegate

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    return [self _heightForCommonCellAtIndexPath:indexPath];
}

- (CGFloat)_heightForCommonCellAtIndexPath:(NSIndexPath *)indexPath {
    static TTPTweetCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:kTweetCellIdentifier];
    });
    
    TTPTweet *tweet = [_tweets objectAtIndex:indexPath.row];
    [sizingCell configure:tweet showAvatar:_showAvatars];
    
    sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tableView.frame), CGRectGetHeight(sizingCell.bounds));
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1.0f; // 1.0f - for cell separator
}

// improve performance
- (CGFloat)tableView:(UITableView*)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath*)indexPath {
    return kDefaultCellHeight;
}

#pragma mark -
#pragma mark Private

- (void)_loadTweets {
    __weak typeof(self) weakSelf = self;
    [[TTPModelFacade sharedModel] getTweetsForQuery:kRequestString withCallback:^(NSArray *tweets) {
        __strong typeof(self) strongSelf = weakSelf;
        [strongSelf _insertNewTweets:tweets];
    }];
}

// animated insert new cells
- (void)_insertNewTweets:(NSArray*)tweets {
    NSInteger newCount = tweets.count - _tweets.count;
    _tweets = tweets;
    if (newCount > 0) {
        NSMutableArray *indexes = [NSMutableArray new];
        for (int i = 0;i<newCount;i++) {
            [indexes addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        }
        [self.tableView insertRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationTop];
    }
}

- (void)_updateVisibleCells {
    // animated update constraints for visible cells
    NSArray *paths = [self.tableView indexPathsForVisibleRows];
    for (NSIndexPath *path in paths) {
        TTPTweetCell *cell = (TTPTweetCell*)[self.tableView cellForRowAtIndexPath:path];
        [cell updateShowAvatar:_showAvatars];
    }
    // animated update cells height
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

@end
