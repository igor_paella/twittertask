//
//  TTPBaseRepository.h
//  TwitterTestProject
//
//  Created by Игорь on 09/04/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabaseQueue.h"

@interface TTPBaseRepository : NSObject

/**
 Queue for thread safety
 */
@property (nonatomic, strong, readonly) FMDatabaseQueue *queue;

@end
