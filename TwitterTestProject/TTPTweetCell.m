//
//  TTPTweetCell.m
//  TwitterTestProject
//
//  Created by Игорь on 27/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import "TTPTweetCell.h"
#import "TTPModelFacade.h"

@interface TTPTweetCell ()
    @property (nonatomic, copy) NSString *url;
@end

@implementation TTPTweetCell

static const int kAvatarWidthConstraintDefault = 60;
static const int kAvatarLeftConstraintDefault = 8;

- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    
}

- (void)configure:(TTPTweet*)tweet showAvatar:(BOOL)showAvatar {
    self.url = tweet.userAvatarUrl;
    self.name.text = tweet.userName;
    self.text.text = tweet.text;
    [self _updateShowAvatar:showAvatar isInit:YES];
    [self _setDate:tweet.date];

    if (showAvatar) {
        [self _loadAndShowAvatar];
    }
}

- (void)updateShowAvatar:(BOOL)showAvatar {
    [self _updateShowAvatar:showAvatar isInit:NO];
}

#pragma mark -
#pragma mark Private

- (void)_updateShowAvatar:(BOOL)showAvatar isInit:(BOOL)init {
    self.avatarWidthConstraint.constant = showAvatar ? kAvatarWidthConstraintDefault : 0;
    self.avatarLeftConstraint.constant = showAvatar ? kAvatarLeftConstraintDefault : 0;
    if (!init) {
        [UIView animateWithDuration:0.7
                         animations:^{
                             [self.contentView layoutIfNeeded];
                         }];
        if (!self.avatar.image) {
            [self _loadAndShowAvatar];
        }
    }
}

- (void)_loadAndShowAvatar {
    __weak typeof(self) weakSelf = self;
    self.avatar.image = nil;
    [[TTPModelFacade sharedModel] imageForUrl:self.url withCallback:^(NSString *sourceUrl, NSData *avatarData) {
        __strong typeof(self) strongSelf = weakSelf;
        if (strongSelf) {
            if ([strongSelf.url isEqualToString:sourceUrl]) {
                [strongSelf _setAvatarWithData:avatarData];
            }
        }
    }];
}

- (void)_setAvatarWithData:(NSData*)data {
    self.avatar.image = [UIImage imageWithData:data];
    self.avatar.clipsToBounds = YES;
}

- (void)_setDate:(NSDate*)date {
    static dispatch_once_t dispatchToken;
    static NSDateFormatter *formatter;
    dispatch_once(&dispatchToken, ^{
        formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"dd.MM HH:mm"];
    });
    self.date.text = [formatter stringFromDate:date];
}

@end
