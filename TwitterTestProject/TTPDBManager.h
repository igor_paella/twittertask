//
//  TTPDBManager.h
//  TwitterTestProject
//
//  Created by Игорь on 28/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTPDBManager : NSObject

/**
 Check database need copy to local directory
 */
+ (void)checkCopyDatabase;

/**
 Get path to database in local directory.
 @return Database path.
 */
+ (NSString*)pathToDB;

@end
