//
//  TTPBaseRepository.m
//  TwitterTestProject
//
//  Created by Игорь on 09/04/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import "TTPBaseRepository.h"
#import "TTPDBManager.h"

@implementation TTPBaseRepository

static dispatch_once_t dispatchToken;
static FMDatabaseQueue *_sharedQueue;

- (FMDatabaseQueue*)queue {
    dispatch_once(&dispatchToken, ^{
        _sharedQueue = [FMDatabaseQueue databaseQueueWithPath:[TTPDBManager pathToDB]];
    });
    return _sharedQueue;
}

@end
