//
//  TTPTweetRepository.m
//  TwitterTestProject
//
//  Created by Игорь on 28/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import "TTPTweetRepository.h"
#import "FMDatabase.h"
#import "FMResultSet.h"
#import "FMDatabaseQueue.h"
#import "TTPDBManager.h"
#import "TTPTweet.h"

@implementation TTPTweetRepository {
}

- (NSArray*)allTweets {
    NSMutableArray *result = [NSMutableArray new];
    [self.queue inDatabase:^(FMDatabase *db) {
        FMResultSet *results = [db executeQuery:@"SELECT * FROM Tweets ORDER BY date DESC"];
        while([results next]) {
            TTPTweet *tweet = [TTPTweet new];
            tweet.id = [results stringForColumn:@"id"];
            tweet.userName = [results stringForColumn:@"userName"];
            tweet.userAvatarUrl = [results stringForColumn:@"userAvatarUrl"];
            tweet.text = [results stringForColumn:@"text"];
            tweet.date = [results dateForColumn:@"date"];
            [result addObject:tweet];
        }
    }];
    return result;
}

- (BOOL)insertTweetIfNotExist:(TTPTweet*)tweet {
    __block BOOL success = NO;
    [self.queue inDatabase:^(FMDatabase *db) {
        FMResultSet *existResults = [db executeQuery:@"SELECT COUNT(*) FROM Tweets WHERE id = ?", tweet.id];
        if ([existResults next]) {
            int tweetsCount = [existResults intForColumnIndex:0];
            [existResults close];
            if (tweetsCount == 0) {
                success = [db executeUpdate:@"INSERT INTO Tweets (id,userName,userAvatarUrl,text,date) VALUES (?,?,?,?,?);",
                                tweet.id, tweet.userName, tweet.userAvatarUrl, tweet.text, tweet.date, nil];
            }
        }
    }];
    return success;
}

- (NSData*)imageDataForUrl:(NSString*)url {
    __block NSData *result = nil;
    [self.queue inDatabase:^(FMDatabase *db) {
        FMResultSet *results = [db executeQuery:@"SELECT * FROM ImageCache WHERE url = ? LIMIT 1", url];
        if ([results next]) {
            result = [results dataForColumn:@"data"];
        }
        [results close];
    }];
    
    return result;
}

- (BOOL)saveImageData:(NSData*)data forUrl:(NSString*)url {
    __block BOOL result;
    [self.queue inDatabase:^(FMDatabase *db) {
        result = [db executeUpdate:@"INSERT OR REPLACE INTO ImageCache (url, data) values (?,?);", url, data];
    }];
    return result;
}

@end
