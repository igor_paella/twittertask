//
//  TTPTweetCell.h
//  TwitterTestProject
//
//  Created by Игорь on 27/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTPTweet.h"
#import "TTPLabel.h"

@interface TTPTweetCell : UITableViewCell

/**
 Avatar image
 */
@property (nonatomic, weak) IBOutlet UIImageView *avatar;

/**
 Label with tweet author name
 */
@property (nonatomic, weak) IBOutlet UILabel *name;

/**
 Label with tweet date
 */
@property (nonatomic, weak) IBOutlet UILabel *date;

/**
 Label with tweet message
 */
@property (nonatomic, weak) IBOutlet TTPLabel *text;

/**
 Avatar image width constraint
 */
@property (nonatomic, weak) IBOutlet NSLayoutConstraint* avatarWidthConstraint;

/**
 Avatar image leading space constraint
 */
@property (nonatomic, weak) IBOutlet NSLayoutConstraint* avatarLeftConstraint;

/**
 Configure cell
 @param tweet Tweet object for cell.
 @param showAvatar Need to show avatar?
 */
- (void)configure:(TTPTweet*)tweet showAvatar:(BOOL)showAvatar;

/**
 Change avatar hidden and text position
 @param showAvatar Need to show avatar?
 */
- (void)updateShowAvatar:(BOOL)showAvatar;

@end
