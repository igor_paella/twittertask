//
//  TTPSettingsRepository.m
//  TwitterTestProject
//
//  Created by Игорь on 28/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import "TTPSettingsRepository.h"
#import "FMDatabase.h"
#import "FMResultSet.h"
#import "FMDatabaseQueue.h"
#import "TTPDBManager.h"


@implementation TTPSettingsRepository {
}

- (TTPSetting*)getSettingForKey:(TTPSettingsKey)key withDefault:(NSNumber*)defaultValue {
    TTPSetting *setting = [TTPSetting new];
    setting.key = key;
    [self.queue inDatabase:^(FMDatabase *db) {
        FMResultSet *results = [db executeQuery:@"SELECT * FROM Settings WHERE key = ? LIMIT 1", [NSNumber numberWithInt:key]];
        if ([results next]) {
            setting.value = [results intForColumn:@"value"];
        } else {
            setting.value = defaultValue.intValue;
        }
        [results close];
    }];
    
    return setting;
}

- (void)setSettingValue:(NSNumber*)value forKey:(TTPSettingsKey)key {
    [self.queue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:@"INSERT OR REPLACE INTO Settings (key, value) values (?,?);",
         [NSNumber numberWithInt:key], value, nil];
    }];
}

@end
