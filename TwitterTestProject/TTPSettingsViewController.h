//
//  TTPSettingsViewController.h
//  TwitterTestProject
//
//  Created by Игорь on 28/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTPSettingsViewController : UIViewController

/**
 Show avatar Switch
 */
@property (nonatomic, weak) IBOutlet UISwitch *showAvatarsSwitch;

@end
