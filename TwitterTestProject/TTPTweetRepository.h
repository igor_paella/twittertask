//
//  TTPTweetRepository.h
//  TwitterTestProject
//
//  Created by Игорь on 28/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTPBaseRepository.h"
#import "TTPTweet.h"

@interface TTPTweetRepository : TTPBaseRepository

/**
 Get all tweets from storage.
 @return TTPTweet array.
 */
- (NSArray*)allTweets;

/**
 Add tweet to storage if not exist.
 @param tweet Tweet for checkin.
 @return Operation success.
 */
- (BOOL)insertTweetIfNotExist:(TTPTweet*)tweet;

/**
 Get image data for url from storage.
 @param url Image url as string.
 @return Image data.
 */
- (NSData*)imageDataForUrl:(NSString*)url;

/**
 Save image data to storage.
 @param data Image data.
 @param forUrl Image url.
 @return Operation success.
 */
- (BOOL)saveImageData:(NSData*)data forUrl:(NSString*)url;

@end
