//
//  TTPParser.h
//  TwitterTestProject
//
//  Created by Игорь on 28/04/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTPTweet.h"

@interface TTPParser : NSObject

- (TTPTweet*)parseTweetWithDictionary:(NSDictionary*)dict;

@end
