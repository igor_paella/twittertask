//
//  TTPSetting.m
//  TwitterTestProject
//
//  Created by Игорь on 09/04/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import "TTPSetting.h"

@implementation TTPSetting

- (BOOL)boolValue {
    return _value == 0 ? NO : YES;
}

@end
