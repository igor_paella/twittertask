//
//  TTPTweet.h
//  TwitterTestProject
//
//  Created by Игорь on 27/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTPTweet : NSObject

@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *userAvatarUrl;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, strong) NSDate *date;

@end
