//
//  TTPModelFacade.m
//  TwitterTestProject
//
//  Created by Игорь on 28/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import "TTPModelFacade.h"
#import "TTPNetworkClient.h"
#import "TTPTweet.h"
#import "TTPTweetRepository.h"
#import "TTPParser.h"

@implementation TTPModelFacade {
    TTPNetworkClient *_client;
    TTPTweetRepository *_repository;
    TTPParser *_parser;
}

- (instancetype)init {
    if ((self =[super init])) {
        [self setup];
    }
    return self;
}

- (void)setup {
    _client = [TTPNetworkClient new];
    _repository = [TTPTweetRepository new];
    _parser = [TTPParser new];
}

+ (instancetype)sharedModel {
    static TTPModelFacade *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark -
#pragma mark Public

- (void)getTweetsForQuery:(NSString*)query withCallback:(void(^)(NSArray*))callback {
    [self _passAllTweets:callback];
    static NSString *bearerToken = nil;
    
    if (bearerToken) {
        [self _processTweetsFromNetworkForQuery:query withBearer:bearerToken withCallback:callback];
    } else {
        [_client updateBearerToken:^(NSString *token) {
            bearerToken = token;
            [self _processTweetsFromNetworkForQuery:query withBearer:bearerToken withCallback:callback];
        }];
    }
}

- (void)imageForUrl:(NSString*)url withCallback:(void(^)(NSString*, NSData*))callback {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        NSData *cachedData = [_repository imageDataForUrl:url];
        if (!cachedData) {
            cachedData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:url]];
            [_repository saveImageData:cachedData forUrl:url];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(url, cachedData);
        });
    });
}

#pragma mark -
#pragma mark Private

- (void)_processTweetsFromNetworkForQuery:(NSString*)query withBearer:(NSString*)bearerToken withCallback:(void(^)(NSArray*))callback {
    [_client getTweetsForQuery:query withBearer:bearerToken withCallback:^(NSDictionary* dict){
        [self _parseTweets:dict];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self _passAllTweets:callback];
        });
    }];
}

- (void)_passAllTweets:(void(^)(NSArray*))callback {
    NSArray *allTweets = [_repository allTweets];
    callback(allTweets);
}

- (void)_parseTweets:(NSDictionary*)dict {
    NSArray *statuses = (NSArray*)dict[@"statuses"];
    for (NSDictionary *entity in statuses) {
        TTPTweet *tweet = [_parser parseTweetWithDictionary:entity];
        [_repository insertTweetIfNotExist:tweet];
    }
}


@end
