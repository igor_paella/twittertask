//
//  TTPSettingsViewController.m
//  TwitterTestProject
//
//  Created by Игорь on 28/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import "TTPSettingsViewController.h"
#import "TTPSettingsRepository.h"

@implementation TTPSettingsViewController {
    TTPSettingsRepository *_settings;
}

- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    _settings = [TTPSettingsRepository new];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.showAvatarsSwitch.on = [_settings getSettingForKey:TTPSettingsKeyShowAvatars withDefault:[NSNumber numberWithBool: YES]].boolValue;
    [self.showAvatarsSwitch addTarget:self action:@selector(showAvatarsChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void)showAvatarsChanged:(UISwitch*)sender {
    [_settings setSettingValue:[NSNumber numberWithBool:self.showAvatarsSwitch.isOn] forKey:TTPSettingsKeyShowAvatars];
}

@end
