//
//  TTPModelFacade.h
//  TwitterTestProject
//
//  Created by Игорь on 28/03/15.
//  Copyright (c) 2015 paella. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTPTweet.h"

@interface TTPModelFacade : NSObject

/**
 Get tweets for query string.
 @param query Query string.
 @param callback A block object to be executed when tweets come from storage or server. Argument: TTPTweet array.
 */
- (void)getTweetsForQuery:(NSString*)query withCallback:(void(^)(NSArray*))callback;

/**
 Get image for url.
 @param url Image url.
 @param callback A block object to be executed when image come from storage or server. Arguments: image url, image data.
 */
- (void)imageForUrl:(NSString*)url withCallback:(void(^)(NSString*, NSData*))callback;

/**
 Returns singleton `TTPModelFacade` object.
 */
+ (instancetype)sharedModel;

@end
